from models.count_down import CountDown
from models.stop_watch import StopWatch
import consts.consts as cnst

if __name__ == '__main__':

    start_second = input("Enter the time in seconds: ")
    counter = CountDown()
    counter.count_down(int(start_second), cnst.TIMER_RESOLUTION, cnst.TIMER_PRECISION)

    stop_watch = StopWatch()
    stop_watch.start(cnst.STOP_WATCH_RESOLUTION, cnst.TIMER_PRECISION)
