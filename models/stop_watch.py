import time

class StopWatch:
    def __init__(self):
        pass

    def start(self, resolution: float, precision: int):
        start_time = time.time()
        while True:
            print("\r" + str(round(time.time() - start_time, precision)), end="")
            time.sleep(resolution)
