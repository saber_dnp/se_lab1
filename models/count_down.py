import math
import time


class CountDown:
    def __init__(self):
        pass

    def count_down(self, t: int, resolution: float, precision: int):
        while t:
            mins, secs = divmod(t, 60)
            try:
                mins_len = int(math.log10(mins)) + 1
            except Exception as e:
                mins_len = 1

            try:
                secs_len = int(math.log10(secs)) + 1
            except Exception as e:
                secs_len = 1

            timer = '0' * max(0, precision - mins_len) + str(mins) + ":" + '0' * max(0, precision - secs_len) + str(secs)
            print('\r' + timer, end="")
            time.sleep(resolution)
            t -= resolution

        print("\n\n\n")
        print('Lift off!')
